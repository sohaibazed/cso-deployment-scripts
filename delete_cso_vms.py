#from __future__ import print_function
import read_config
import paramiko



def run_delete_vms_script(physical_servers):
    client = paramiko.SSHClient()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    for server_name,server_properties in physical_servers.items():
        address = server_properties['management_address']
        username = server_properties['username']
        password = server_properties['password']

        print("-----------------")
        print(server_name)
        print("-----------------")
        client.connect(address, username=username, password=password)
        sftp = client.open_sftp()
        sftp.put('delete_vms.sh', '/tmp/delete_vms.sh')
        sftp.close()
        
        stdin, stdout, stderr = client.exec_command('sh /tmp/delete_vms.sh')
        for line in stdout:
            print(line)

        stdin, stdout, stderr = client.exec_command('rm /tmp/delete_vms.sh')
        for line in stdout:
            print(line)

    client.close()


def run():
    print "hahah deleting vms"
    physical_servers = read_config.get_physical_server_list()
    run_delete_vms_script(physical_servers)


