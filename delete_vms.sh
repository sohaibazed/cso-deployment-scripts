#!/usr/bin/env bash
echo -n "Delete all VMs? (CTRL+C to quit)"
for VM in $(virsh list --all | awk '{print $2}' | grep -v Name); do virsh destroy $VM; virsh undefine $VM; done

rm -rf /root/disks
rm -rf /root/ubuntu_vm
rm -rf /root/disk_can/
