from oslo_config import cfg
import sys

def get_physical_server_list():
    opt_group = cfg.OptGroup(name='TARGETS', title='A Simple Example')
    target_opts = [cfg.ListOpt('physical', default=[], help=('True enables, False disables')), cfg.ListOpt('server', default=[], help=('True enables, False disables'))]
    CONF = cfg.CONF
    CONF.register_group(opt_group)
    CONF.register_opts(target_opts, opt_group)
    list = {}
    print sys.argv[1]
    CONF(['--config-file', 'provision_vm.conf'])
    for physical_server in CONF['TARGETS']['physical']:
        variable_group = cfg.OptGroup(name=physical_server, title='A Simple Example')
        variable_opts = [ cfg.StrOpt('management_address', default='', help=('True enables, False disables')) , cfg.StrOpt('username', default='', help=('True enables, False disables')) , cfg.StrOpt('password', default='', help=('True enables, False disables'))]
        vCONF = cfg.CONF
        vCONF.register_group(variable_group)
        vCONF.register_opts(variable_opts, variable_group)
        management_address_with_subnet = vCONF[physical_server]['management_address']
        management_address,subnet = management_address_with_subnet.split("/")
        properties = { 'management_address':management_address , 'username':vCONF[physical_server]['username'] , 'password':vCONF[physical_server]['password'] }
        list[physical_server] = properties
    return list

def get_virtual_machine_list():
    opt_group = cfg.OptGroup(name='TARGETS', title='A Simple Example')
    target_opts = [cfg.ListOpt('physical', default=[], help=('True enables, False disables')), cfg.ListOpt('server', default=[], help=('True enables, False disables'))]
    CONF = cfg.CONF
    CONF.register_group(opt_group)
    CONF.register_opts(target_opts, opt_group)
    list = {}
    CONF(['--config-file', 'provision_vm.conf'])
    for virtual_server in CONF['TARGETS']['server']:
        variable_group = cfg.OptGroup(name=virtual_server, title='A Simple Example')
        variable_opts = [ cfg.StrOpt('management_address', default='', help=('True enables, False disables')) , cfg.StrOpt('username', default='', help=('True enables, False disables')) , cfg.StrOpt('password', default='', help=('True enables, False disables'))]
        vCONF = cfg.CONF
        vCONF.register_group(variable_group)
        vCONF.register_opts(variable_opts, variable_group)
        management_address_with_subnet = vCONF[virtual_server]['management_address']
        management_address,subnet = management_address_with_subnet.split("/")
        properties = { 'management_address':management_address , 'username':vCONF[virtual_server]['username'] , 'password':vCONF[virtual_server]['password'] }
        list[virtual_server] = properties
    return list