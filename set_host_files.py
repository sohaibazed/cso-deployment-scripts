import read_config
import paramiko


def set_host_file(physical_servers):
    client = paramiko.SSHClient()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    for server_name, server_properties in physical_servers.items():
        if server_name == "csp-vrr-vm":
            continue
        address = server_properties['management_address']
        username = server_properties['username']
        password = server_properties['password']
        print(address)
        client.connect(address, username=username, password=password)
        sftp = client.open_sftp()
        sftp.put('host_file.txt', '/tmp/host_file.txt')
        sftp.close()

        stdin, stdout, stderr = client.exec_command(
            "sed '/###<!-- BOF Added by automation script -->/,/###<!-- EOF Added by automation script -->/d' /etc/hosts >> /tmp/host_file_original.txt")
        for line in stdout:
            print(line)

        stdin, stdout, stderr = client.exec_command(
            'cat /tmp/host_file_original.txt /tmp/host_file.txt >> /tmp/host_file_new.txt')
        for line in stdout:
            print(line)

        stdin, stdout, stderr = client.exec_command('cp /tmp/host_file_new.txt /etc/hosts')
        for line in stdout:
            print(line)

        stdin, stdout, stderr = client.exec_command('rm /tmp/host_file*')
        for line in stdout:
            print(line)

    client.close()


def run():

    print "hahahaah running set_host_files"
    physical_servers = read_config.get_physical_server_list()
    virtual_server = read_config.get_virtual_machine_list()
    set_host_file(physical_servers)
    set_host_file(virtual_server)
