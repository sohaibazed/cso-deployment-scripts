Delete VMs on phyiscal servers. 
To do so, the script will read provison_vm.conf file in the directroy to populate the list of physical servers, log in over ssh and delete VMs

    python main.py delete_cso_vms


Set host files
To do so, the script will read provison_vm.conf and host_files.txt file in the directroy to populate the list of physical servers as well as VMs, log in over ssh and update host files.

    python main.py set_host_file