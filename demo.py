import read_config


if __name__ == "__main__":
    physical_servers = read_config.get_physical_server_list()
    virtual_machines = read_config.get_virtual_machine_list()

    print(physical_servers)
    print("")
    print(virtual_machines)
