import set_host_files
import delete_cso_vms
import sys
import argparse



if __name__ == "__main__":

    parser = argparse.ArgumentParser()

    parser.add_argument('--option1', help='description for option1')
    parser.add_argument('--option2', help='description for option2')
    parser.add_argument('--option3', help='description for option3')


    if len(sys.argv) != 2:
        print "Enter a valid arguement"
        exit()
    else:
        if str(sys.argv[1]) == "set_host_file":
            set_host_files.run()
        elif sys.argv[1] == "delete_cso_vms":
            delete_cso_vms.run()
